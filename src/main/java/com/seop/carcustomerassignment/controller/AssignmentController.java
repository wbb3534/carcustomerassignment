package com.seop.carcustomerassignment.controller;

import com.seop.carcustomerassignment.model.AssignmentItem;
import com.seop.carcustomerassignment.model.CommonResult;
import com.seop.carcustomerassignment.model.ListResult;
import com.seop.carcustomerassignment.service.AssignmentService;
import com.seop.carcustomerassignment.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@Api(tags = "고객 배분")
@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/assignment")
public class AssignmentController {
    private final AssignmentService assignmentService;

    @ApiOperation(value = "고객배분 등록")
    @PostMapping("/new/{customerId}/{salesManId}")
    public CommonResult setAssignment(@PathVariable long customerId, @PathVariable long salesManId) {
        assignmentService.setAssignment(customerId, salesManId);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "고객배분 리스트")
    @GetMapping("/list")
    public ListResult<AssignmentItem> getCustomersInfo() {
        return ResponseService.getListResult(assignmentService.getAssignments(), true);
    }
}
