package com.seop.carcustomerassignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarCustomerAssignmentApplication {

    public static void main(String[] args) {
        SpringApplication.run(CarCustomerAssignmentApplication.class, args);
    }

}
