package com.seop.carcustomerassignment.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
