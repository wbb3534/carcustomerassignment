package com.seop.carcustomerassignment.service;

import com.seop.carcustomerassignment.entity.Assignment;
import com.seop.carcustomerassignment.model.AssignmentItem;
import com.seop.carcustomerassignment.model.ListResult;
import com.seop.carcustomerassignment.repository.AssignmentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AssignmentService {
    private final AssignmentRepository assignmentRepository;

    public void setAssignment(long customerId, long salesManId) {
        Assignment addData = new Assignment.AssignmentBuilder(customerId, salesManId).build();

        assignmentRepository.save(addData);
    }

    public ListResult<AssignmentItem> getAssignments() {
        List<Assignment> originData = assignmentRepository.findAll();

        List<AssignmentItem> result = new LinkedList<>();

        originData.forEach(item -> result.add(new AssignmentItem.AssignmentItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }
}
