package com.seop.carcustomerassignment.entity;

import com.seop.carcustomerassignment.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Assignment {
    @ApiModelProperty(value = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ApiModelProperty(value = "고객 시퀀스")
    @Column(nullable = false)
    private Long customerId;
    @ApiModelProperty(value = "영업사원 시퀀스")
    @Column(nullable = false)
    private Long salesManId;
    @ApiModelProperty(value = "배정시간")
    @Column(nullable = false)
    private LocalDateTime assignmentTime;

    private Assignment(AssignmentBuilder builder) {
        this.customerId = builder.customerId;
        this.salesManId = builder.salesManId;
        this.assignmentTime = builder.assignmentTime;
    }
    public static class AssignmentBuilder implements CommonModelBuilder<Assignment> {
        private final Long customerId;
        private final Long salesManId;
        private final LocalDateTime assignmentTime;

        public AssignmentBuilder(long customerId, long salesManId) {
            this.customerId = customerId;
            this.salesManId = salesManId;
            assignmentTime = LocalDateTime.now();
        }
        @Override
        public Assignment build() {
            return new Assignment(this);
        }
    }
}
