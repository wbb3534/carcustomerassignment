package com.seop.carcustomerassignment.repository;

import com.seop.carcustomerassignment.entity.Assignment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AssignmentRepository extends JpaRepository<Assignment, Long> {
}
