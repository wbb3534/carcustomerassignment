package com.seop.carcustomerassignment.model;

import com.seop.carcustomerassignment.entity.Assignment;
import com.seop.carcustomerassignment.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AssignmentItem {
    @ApiModelProperty(value = "시퀀스")
    private Long id;
    @ApiModelProperty(value = "고객 시퀀스")
    private Long customerId;
    @ApiModelProperty(value = "영업사원 시퀀스")
    private Long salesManId;
    @ApiModelProperty(value = "배정시간")
    private LocalDateTime assignmentTime;

    private AssignmentItem(AssignmentItemBuilder builder) {
        this.id = builder.id;
        this.customerId = builder.customerId;
        this.salesManId = builder.salesManId;
        this.assignmentTime = builder.assignmentTime;
    }
    public static class AssignmentItemBuilder implements CommonModelBuilder<AssignmentItem> {
        private final Long id;
        private final Long customerId;
        private final Long salesManId;
        private final LocalDateTime assignmentTime;

        public AssignmentItemBuilder(Assignment assignment) {
            this.id = assignment.getId();
            this.customerId = assignment.getCustomerId();
            this.salesManId = assignment.getSalesManId();
            this.assignmentTime = assignment.getAssignmentTime();
        }
        @Override
        public AssignmentItem build() {
            return new AssignmentItem(this);
        }
    }
}
